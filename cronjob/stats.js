'use strict';

//module that should be run as cronjob every hour at 10 minutes
const config = require('../config/config');
const userSyncDbConnection = config.user_sync_db.connection;

const mysql = require('mysql');
const mysqlConnection = mysql.createConnection(userSyncDbConnection);
const connection = config.cache.connection;

const redis = require('redis');
const client = redis.createClient(connection);

client.on("error", err => {
  console.error("Redis client error.\n%s \n%s \n%s", err.message, err.code, err.stack);
});

mysqlConnection.connect(err => {
  if (err) {
    mysqlConnection.end();
    console.error('Mysql connection error.', err);
    process.exit(0);
  }
  console.log('Mysql connection successfully established.');
});

console.log('Script started.');
mysqlQuery(`select id, partner_name from partners where is_active = 1 and region = '${config.region}'`)
  .then(partners => {
    const promises = [];

    partners.forEach(partner => {
      promises.push(updatePartnerStats(partner));
    });

    Promise.all(promises).then(results => {
      //results array contains errors which have been returned from promises
      const hasErrors = results.reduce((previousValue, currentValue) => {
        return previousValue || currentValue;
      });

      if (hasErrors) {
        console.error('Not all partners\' stats were processed successfully!');
      } else {
        console.log('All partners\' stats were processed successfully!');
      }

      closeConnections();
    });

  })
  .catch(err => {
    console.error(err);
    closeConnections();
  });

function updatePartnerStats(partner) {
  const key = getStatsKey(partner.id);

  return new Promise((resolve, reject) => {
    client.hgetall(key, (err, stats) => {
      if (err) {
        console.error(`Error while getting ${partner.partner_name} stats from cache.`, err);
        return resolve(err);
      }

      if (stats === null) {
        console.log(`${partner.partner_name} stats is empty for key \'${key}\'`);
        return resolve(null);
      }

      const date = new Date(Date.now() - (1000 * 60 * 60)); //get unix time 1 hour ago
      const utcHour = date.getUTCHours();

      let formattedDate = date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate() + ' ';
      formattedDate += (utcHour >= 10 ? utcHour : '0' + utcHour) + ':00:00';

      const query = `INSERT INTO hourly_stats (
                    partner_id, 
                    new_users, 
                    exist_users, 
                    invalid_keys, 
                    invalid_redirs, 
                    errors, 
                    date) 
                  VALUES (
                    ${partner.id},
                    ${+stats.new_users || 0},
                    ${+stats.exist_users || 0},
                    ${+stats.invalid_keys || 0},
                    ${+stats.invalid_redirs || 0},
                    ${+stats.errors || 0},
                    '${formattedDate}')
                  ON DUPLICATE KEY UPDATE 
                    new_users = new_users + ${+stats.new_users || 0},
                    exist_users = exist_users + ${+stats.exist_users || 0},
                    invalid_keys = invalid_keys + ${+stats.invalid_keys || 0},
                    invalid_redirs = invalid_redirs + ${+stats.invalid_redirs || 0},
                    errors = errors + ${+stats.errors || 0}`;

      mysqlQuery(query)
        .then(() => {
          client.del(key, (err) => {
            if (err) {
              console.error(`Error while deleting stats from cache for key '${key}', partner - ${partner.partner_name}`);
              return resolve(err);
            }

            return resolve(null);
          });
        })
        .catch(err => resolve(err));
    });
  });
}

function getStatsKey(partnerId) {
  let key = partnerId + ':';
  const date = new Date(Date.now() - (1000 * 60 * 60)); //get unix time 1 hour ago
  key += date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate() + ':' + date.getUTCHours();

  return key;
}

function mysqlQuery(query) {
  return new Promise((resolve, reject) => {
    mysqlConnection.query(query, (err, rows) => {
      if (err) {
        console.error('Error while executing query', err);
        return reject(err);
      }

      if (rows) return resolve(rows);
      else return resolve(true);
    });
  });
}

function closeConnections() {
  mysqlConnection.end(err => {
    if (err) console.error(err);
  });
  client.quit();
  console.log('Script ended.');
}