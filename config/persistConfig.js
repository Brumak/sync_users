'use strict';

/*
Here you must add variables and constants which should be depend on system version.
E.g. ip address of current servers might be changed for each server, so this variable must be placed in ./config.js.dist

If you want to add path(relative) to local files or something like that - save it here
 */
let key = '';
const keysTransforms = {
  user_key: {
    secretKey: Buffer.alloc(16, `${key}`, 'hex'),
    chipAlgorithm: 'aes128',
    hash: 'hex'
  },
};

module.exports = {
  partnersConfigFile: 'partners.json',
  storageServersConfigFile: 'storageServers.json',
  keysTransforms: keysTransforms
};