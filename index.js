'use strict';

const http = require('http');
const https = require('https');
const fs = require('fs');
const { port, secure_port } = require('./config/config.js');
const router = require('./app/modules/routes').router;
const init = require('./app/modules/init');

// const pm2 = require('pm2');

process.on('SIGINT', function() { //close all connections here (aerospike, redis, mysql(if needed))
  init.closeConnections((err) => {
    if (err) {
      console.error('\n' + 'Error while closing all connections.', err);
    }
    process.exit(err ? 1 : 0);
  });
});

init.run();

const optionsSSL = {
    "key":  fs.readFileSync(__dirname + '/cert/cert.com.key'),
    "cert": fs.readFileSync(__dirname + '/cert/cert.com.crt'),
    "ca":   fs.readFileSync(__dirname + '/cert/cert.com.chain.crt')
};
http.createServer((req, res) => router(req, res, 'http')).listen(port);
https.createServer(optionsSSL, (req, res) => router(req, res, 'https')).listen(secure_port);