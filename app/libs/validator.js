'use strict';

const config = require('../../config/config');
const { USER_SYNC, PARTNER } = require('./constants').mappingSides;

module.exports = {
  isValidPartnerUid(partnerUid) {
    if (typeof partnerUid === 'string') {
      return partnerUid.length <= config.partner_uid_max_length;
    } else {
      return false;
    }
  },

  isValidUrl(url) {
    let regexp = /^(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
    return regexp.test(url);
  },

  hasNeededMacroses(str, macroses, partner) {
    for (let macro of macroses)
      if (!str.includes(macro)) {
        console.error(`Error: needed macroses ${macroses} don\'t exist in partner redirect url`
          + ` - ${str}, partner - ${partner.partner_name}`);
        return false;
      }

    return true;
  },

  checkPartnerSettings(partner) {
    //checking for correct partner settings
    if (partner.pixel_calling_side === USER_SYNC && partner.keys_store_side === PARTNER) {
      if (!partner.partner_url) {
        console.error('Error: you have partner ' + partner.partner_name
          + ' for whom we call its pixel but partner url is not defined');
        return false;
      }

      if (partner.partner_url.indexOf(config.default_uid_macros) === -1) {
        console.error('Error: you have partner ' + partner.partner_name
          + ' who stores keys on our side but uid macros doesn\'t exist in redirect url - ' + partner.partner_url);
        return false;
      }
    }

    return true;
  }
};