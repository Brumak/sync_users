'use strict';

module.exports = {
  statsProps: {
    INVALID_REDIRS: 'invalid_redirs',
    INVALID_KEYS: 'invalid_keys',
    EXIST_USERS: 'exist_users',
    NEW_USERS: 'new_users',
    ERRORS: 'errors'
  },

  /*
   This constant must be equal to enum field `keys_store_side` in partners table
   */
  mappingSides: {
    USER_SYNC: "user-sync",
    PARTNER: "partner"
  }
};