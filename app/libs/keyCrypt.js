'use strict';

const uuid = require('uuid');
const crypto = require('crypto');
const keysTransform = require('../../config/persistConfig').keysTransforms;

module.exports = {
  encryptKey(key, partner) {

    if (partner.crypt_type !== 'none' && !keysTransform[partner.crypt_type]) {
      console.error('Platform name is not supported, available: ' + Object.keys(keysTransform) + ', '
        + partner.crypt_type + ' given.');
      return false;
    }

    if (partner.crypt_type === 'none') return key;

    const {secretKey, chipAlgorithm = 'aes128', hash = 'base64'} = keysTransform[partner.crypt_type];

    if (hash !== 'base64' && hash !== 'hex') {
      console.error('Hash value is not valid, hex or base64 are available, ' + hash + ' given.');
      return false;
    }

    const cipher = crypto.createCipher(chipAlgorithm, secretKey);
    let crypted = cipher.update(key, 'hex', hash);
    crypted += cipher.final(hash);
    return crypted;
  }
}