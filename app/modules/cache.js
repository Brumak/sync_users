'use strict';

const props = require('../libs/constants').statsProps;
const {
  connection,
  stats_keys_ttl,
  cache_update_interval
} = require('../../config/config.js').cache;

const redis = require('redis');
const client = redis.createClient(connection);

client.on("error", err => {
  console.error("Redis client error.\n%s \n%s \n%s", err.message, err.code, err.stack);
});

setInterval(updateCacheStats, cache_update_interval);

let tempStats = {}; //unique for each processes, it will be merged in redis

module.exports = {
  incrInvalidRedirs(partnerId) {
    incrementStats(partnerId, props.INVALID_REDIRS);
  },

  incrInvalidKeys(partnerId) {
    incrementStats(partnerId, props.INVALID_KEYS);
  },

  incrExistUsers(partnerId) {
    incrementStats(partnerId, props.EXIST_USERS);
  },

  incrNewUsers(partnerId) {
    incrementStats(partnerId, props.NEW_USERS);
  },

  incrErrors(partnerId) {
    incrementStats(partnerId, props.ERRORS);
  },

  close() {
    client.quit();
  }
};

function incrementStats(partnerId, statKey) {
  const key = getKey(partnerId);
  tempStats[key] = tempStats[key] || {};

  if (!tempStats[key][statKey]) {
    tempStats[key][statKey] = 1;
  } else {
    ++tempStats[key][statKey];
  }
}

function updateCacheStats() {
  Object.keys(tempStats).forEach(key => {
    Object.keys(tempStats[key]).forEach(statKey => {//example: statKey == 'invalid_redirs', 'new_users'

      client.hexists(key, statKey, (err, res) => {
        logError('Error in cache: hexists func', err);

        if (res) {
          client.hincrby(key, statKey, tempStats[key][statKey], (err) => logError('Error in cache: hincrby func.', err));
        } else {
          client.hset(key, statKey, tempStats[key][statKey], (err) => {
            logError('Error in cache: hset func', err);

            client.expire(key, stats_keys_ttl, (err) => logError('Error in cache: expire func.', err));
          });
        }

        delete tempStats[key][statKey];
      });
    });
  });
}

function getKey(partnerId) {
  let key = partnerId + ':';
  const date = new Date();
  key += date.getUTCFullYear() + '-' + (date.getUTCMonth() + 1) + '-' + date.getUTCDate() + ':' + date.getUTCHours();

  return key;
}

function logError(message, err) {
  if (err) {
    console.error(message, err);
  }
}