'use strict';

const fs = require('fs');
const config = require('../../config/config');
const persistConfig = require('../../config/persistConfig');
const { updateSyncedPixelsMarkup } = require('./syncMarkup');
const refreshRoutes = require('./routes').refreshRoutes;
const container = require('./container');
const { checkPartnerSettings } = require('../libs/validator');
const storage = require('./storage');
const cache = require('./cache');
const userSyncDb = require('./userSyncDb');
const pathToConfDir = __dirname + '/../../config/';

function loadPartnersConfig() {
  userSyncDb.query(`select * from partners where is_active = 1 and region = '${config.region}'`, function (err, rows) {
    if (err) console.error('Error while querying partners config from DB.', err);

    //handle rows (write to some files, etc)
    const pixels = {};
    for (let i = 0; i < rows.length; ++i) {
      const row = rows[i];
      const key = row.endpoint;

      if (!checkPartnerSettings(row)) continue;

      pixels[key] = {};
      pixels[key].id = +row.id;
      pixels[key].company_name = row.company_name;
      pixels[key].partner_name = row.partner_name;
      pixels[key].crypt_type = row.crypt_type;
      pixels[key].partner_url = row.partner_url;
      pixels[key].default_handling = row.default_handling;
      pixels[key].keys_store_side = row.keys_store_side;
      pixels[key].pixel_calling_side = row.pixel_calling_side;

      try {
        pixels[key].additional_info = JSON.parse(row.additional_info);
      } catch (err) {
        console.error('Error JSON parsing additional_info field from partners config for ' + row.partner_name, err);
        pixels[key].additional_info = {};
      }
    }

    fs.writeFile(pathToConfDir + persistConfig.partnersConfigFile, JSON.stringify(pixels, null, 2), err => {
      if (err) console.error('Error while writing partners config to file.', err);
    });
  });
}

function loadStorageServersConfig() {
  userSyncDb.query('select * from storage_servers where is_active = 1', function (err, rows) {
    if (err) console.error('Error while querying storage servers config from DB.', err);

    const storageServers = [];

    for (let i = 0; i < rows.length; ++i) {
      let row = rows[i];

      storageServers.push({
        addr: row.ip_address,
        port: row.port
      });
    }

    fs.writeFile(pathToConfDir + persistConfig.storageServersConfigFile, JSON.stringify(storageServers, null, 2), err => {
      if (err) if (err) console.error('Error while writing storage servers config to file.', err);
    });

  });
}

function initMainProcess() {
  userSyncDb.connect()
    .then(() => {
      loadConfigs();
    })
    .catch(err => {
      console.error(err);
    });

  setInterval(loadConfigs, config.config_update_interval);
}

function readPartnersConfig() {
  return new Promise((resolve, reject) => {
    fs.readFile(pathToConfDir + persistConfig.partnersConfigFile, 'utf8', (err, data) => {
      if (err) {
        console.error('Error while reading partners config from file.');
        return reject(err);
      }

      if (data) {
        try {
          const partners = JSON.parse(data);
          container.set('partners', partners);
        } catch (err) {
          container.set('partners', {});
          console.error('Error while parsing partners config from file.');
          return reject(err);
        }

        return resolve(data);
      } else {
        return resolve({});
      }
    });
  });
}

function loadConfigs() {
  loadPartnersConfig();
  loadStorageServersConfig();
}

function readConfigs() {
  /*readStorageServersConfig()
    .catch(err => console.error(err)); //.then(reconnect if added new nodes)*/

  readPartnersConfig()
    .then(updateSyncedPixelsMarkup)
    .then(refreshRoutes)
    .catch(err => console.error(err));
}

function run() {
  if (+process.env.pm_id === 0) {
    initMainProcess(); //load all partners and servers config here
  }

  //for all processes, start reading config with 5 seconds delay
  setTimeout(() => {
    readConfigs();
    setInterval(readConfigs, config.config_update_interval);
  }, 5000);
}

function closeConnections(cb) {
  userSyncDb.close(err => {
    storage.close();
    cache.close();

    cb(err);
  });
}

module.exports = {
  run,
  closeConnections
};