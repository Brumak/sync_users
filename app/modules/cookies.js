'use strict';

const config = require('../../config/config.js');
const userid = require('./userid');

const uuidReg = /^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i;

function getDateExpiry(days = config.cookie_ttl) {
  const date = new Date;
  date.setDate(date.getDate() + days);

  return date.toUTCString();
}

function getCookieInfo(req, name = config.cookie_key, partnerName = 'unknown') {
  const cookieInfo = { isExists: false };

  if (typeof req.headers.cookie !== 'string') {
    cookieInfo.cookieValue = userid.getUserId(req, partnerName);
    return cookieInfo;
  }

  const matches = req.headers.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));

  if (matches) {
    if (matches[1].length !== 40) { //sha1 generates user id with 40 chars length
      cookieInfo.cookieValue = userid.getUserId(req, partnerName);
    } else {
      cookieInfo.isExists = true;
      cookieInfo.cookieValue = decodeURIComponent(matches[1]);
    }
  } else {
    cookieInfo.cookieValue = userid.getUserId(req, partnerName);
  }

  return cookieInfo;
}

module.exports.getCookieInfo = getCookieInfo;
module.exports.getDateExpiry = getDateExpiry;