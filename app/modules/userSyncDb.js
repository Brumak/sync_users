'use strict';

const config = require('../../config/config');
const userSyncDbConnection = config.user_sync_db.connection;
const mysql = require('mysql');
const mysqlConnection = mysql.createConnection(userSyncDbConnection);

function connect() {
  return new Promise((resolve, reject) => {
    mysqlConnection.connect(err => {
      if (err) {
        mysqlConnection.end();
        console.error('Mysql connection error.');
        reject(err);
      }

      resolve(true);
    });
  });
}

function query(query, cb) {
  mysqlConnection.query(query, (err, data) => {
    cb(err, data);
  });
}

function close(cb) {
  mysqlConnection.end(cb);
}

module.exports = {
  connect,
  query,
  close
};