'use strict';

const { USER_SYNC, PARTNER } = require('../libs/constants').mappingSides;
const config = require('../../config/config');
const container = require('./container');

function getPartnerLink(partner, endpoint) {
    let link = null;

    //link needed only if we call pixels for our partners
    if (partner.pixel_calling_side === PARTNER) return link;

    const protocol = partner.secure ? 'https://' : 'http://';
    const fullUserSyncEndpoint = protocol + config.hostname + endpoint;

    if (partner.keys_store_side === USER_SYNC) {
        link = partner.partner_url.replace(config.default_redir_macros, encodeURIComponent(fullUserSyncEndpoint));
    } else {
        link = fullUserSyncEndpoint;
    }

    return link;
}

function escapeLink(link) {
    return '\'' + link + '\'';
}

function updateSyncedPixelsMarkup() {
    const partners = container.get('partners');

    const syncLinks = {};
    for (let key in partners) {
        if (!partners.hasOwnProperty(key)) continue;

        const partner = partners[key];
        const platform = partner.crypt_type;

        if (platform === 'none') continue;

        const partnerLink = getPartnerLink(partner, key);

        if (partner.pixel_calling_side === USER_SYNC && partnerLink) {
            if (!syncLinks[platform]) syncLinks[platform] = [];
            syncLinks[platform].push(escapeLink(partnerLink));
        }
    }
    container.set('syncedPixelsMarkup', syncLinks);

    return Promise.resolve();
}

module.exports.updateSyncedPixelsMarkup = updateSyncedPixelsMarkup;