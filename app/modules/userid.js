'use strict';

const crypto = require('crypto');

function getIpAddress(req) {
  let ip = req.headers['x-forwarded-for'] ||
    req.connection.remoteAddress ||
    req.socket.remoteAddress ||
    (req.connection.socket && req.connection.socket.remoteAddress);

  if (!ip) return '';

  [ip] = ip.split(',');
  ip = ip.replace(/(\:\:ffff\:)/i, '');
  return ip.trim();
}

function getUserAgent(req) {
  let userAgent = (req.headers && req.headers['user-agent']) ? req.headers['user-agent'] : '';
  userAgent = userAgent.toLowerCase().trim();
  return userAgent;
}

module.exports.getUserId = function (req, partnerName) {
  const userAgent = getUserAgent(req);
  const ip = getIpAddress(req);

  if (!userAgent || !ip) {
    console.error(`Error: could not generate userid. Partner name - ${partnerName}, req.headers - `
      + `${JSON.stringify(req.headers)}, req.ip - ${ip}.`);
    return '';
  }

  const userId = userAgent + ip;
  return crypto.createHash('sha1').update(userId).digest('hex');
};