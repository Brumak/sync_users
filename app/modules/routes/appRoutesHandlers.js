'use strict';

const config = require('../../../config/config');
const userSyncDb = require('../userSyncDb');
const { USER_SYNC, PARTNER } = require('../../libs/constants').mappingSides;
const { checkPartnerSettings } = require('../../libs/validator');
const keysTransforms = require('../../../config/persistConfig').keysTransforms;
const container = require('../container');
const cookies = require('../cookies');

const availablePlatforms = {
  'east': true,
  'west': true,
  'singapoor': true
};

module.exports = {
  /*
   API section. Paths here should begin with /api prefix
   */

  '/api/partners_conf': function(req, res) {

    let platform = req.urlParsed.query.platform;
    if (req.urlParsed.query.api_key !== config.api_key) {
      res.statusCode = 403;
      res.end('Forbidden');
      return;
    }

    if (!availablePlatforms[platform]) {
      res.statusCode = 400;
      res.end('Unsupported platform');
      return;
    }

    const query = `SELECT
                    pm.external_id       AS id,
                    pm.internal_id       AS syncId,
                    p.crypt_type         AS crypt_type,
                    p.keys_store_side    AS keys_store_side,
                    p.pixel_calling_side AS pixel_calling_side,
                    p.endpoint           AS endpoint,
                    p.partner_url        AS partner_url,
                    p.secure             AS secure
                  FROM partners_map AS pm LEFT JOIN partners AS p ON pm.internal_id = p.id
                  WHERE pm.platform_name='${platform}'
                  	AND p.is_active=1 AND p.region='${config.region}'`;

    userSyncDb.query(query, (err, data) => {
      if (err) {
        console.error(err, query);
        res.statusCode = 500;
        res.end('Internal server error');
        return;
      }

      const returnedData = {};
      data.forEach(row => {

        if (!checkPartnerSettings(row)) return;

        returnedData[row.id] = {
          syncId: row.syncId,
          storingSide: row.keys_store_side,
          pixelCallingSide: row.pixel_calling_side,
          cryptType: row.crypt_type
        }
      });

      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(returnedData));
    });
  },

  '/api/ciphers': function (req, res) {
    if (req.urlParsed.query.api_key !== config.api_key) {
      res.statusCode = 403;
      res.end('Forbidden');
      return;
    }
    let platform = req.urlParsed.query.platform;

    if (!availablePlatforms[platform]) {
      res.statusCode = 400;
      res.end('Unsupported platform');
      return;
    }

    const cipherData = keysTransforms[platform];
    cipherData.secretKey = cipherData.secretKey.toString('hex');

    const returnedJson = JSON.stringify(cipherData);

    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(returnedJson);
  },

  //End API section

  '/setck': function (req, res) {

    let platform = req.urlParsed.query.p;
    if (!availablePlatforms[platform]) {
      res.statusCode = 400;
      res.end('Unsupported platform');
      return;
    }

    const { cookieValue } = cookies.getCookieInfo(req, config.cookie_key, 'setck');
    res.setHeader("Set-Cookie", `${config.cookie_key}=${cookieValue}; path=/; domain=${config.cookie_domain}; `
        + `expires=${cookies.getDateExpiry()}`);

    const syncedPixelsMarkup = container.get('syncedPixelsMarkup');
    if (!syncedPixelsMarkup) {
      res.statusCode = 204;
      res.end('');
      return;
    }

    const responseJS = `(function(){
                             var syncLinks = [${syncedPixelsMarkup[platform]}];
                             var body = document.getElementsByTagName('body')[0];
                             for (var i = 0; i < syncLinks.length; ++i) {
                                var img = document.createElement('img');
                                img.src = syncLinks[i];
                                img.setAttribute('border', 0);
                                img.setAttribute('width', 1);
                                img.setAttribute('height', 1);
                                img.setAttribute('alt', '');
                                img.style.visibility = 'hidden';
                                body.appendChild(img);
                             }
                         })();`;

    res.statusCode = 200;
    res.setHeader("Content-Type", "application/javascript");
    res.end(responseJS);
  }
};
