'use strict';

const handleUser = require('./defaultFunctions').handleUser;
const prepareCookies = require('./defaultFunctions').prepareCookies;

module.exports = {
  /*
   This handler here just to know how you can add new custom route handlers for custom partners.
   Usually, all you need to change in behavior of custom handler is how user redirection will be processed.
   So you can simply used prepareCookies() function to store and read user's cookies
   and handleUser() function to store user's data in storage (if needed).

   !!!
   Please note that handleUser function requires last argument to be a callback function
   that will be invoked right after storing user to storage. You must call res.end function here to respond correctly.
   !!!
   */
  '/testCustomPartner.gif': function(req, res, partner) {
    const cookieValue = prepareCookies(req, res, partner);

    handleUser(partner, req.urlParsed, cookieValue, cookieValue => {
      res.end();
    });
  },
};
