'use strict';

const handleDefaultPartner = require('./defaultFunctions').handleDefaultPartner;
const config = require('../../../config/config');
const cache = require('./../cache');
const url = require('url');
const fs = require('fs');

const customPartnersRoutesHandlers = require('./customPartnersRoutesHandlers');
const container = require('../container');
const appRoutes = require('./appRoutesHandlers'); //here stored all const app routes (e.g. /api ...)
let partnersRoutes = {}; //here stored all partners routes (pixels)

module.exports = {
  router(req, res, protocol) {
    const urlParsed = url.parse(req.url, true); //someurl?param=value -> parsedUrl.pathname == /someurl, parsedUrl.query
    req.urlParsed = urlParsed;
    req.protocol = protocol;
    const partners = container.get('partners');
    if(urlParsed.pathname === '/ngn567.gif'){
          let log_string = JSON.stringify(urlParsed)+'\n';
          fs.appendFile('/var/www/user-sync/logs/ngin_log.txt', log_string);
    }
    if (partnersRoutes[urlParsed.pathname]) {
      partnersRoutes[urlParsed.pathname](req, res, partners[urlParsed.pathname]);

    } else if (appRoutes[urlParsed.pathname]) {
      appRoutes[urlParsed.pathname](req, res);

    } else {
      res.writeHead(404, {'Content-Type': 'text/plain'});
      res.end('Not Found');
    }
  },

  refreshRoutes() { //called from init module right after partners settings nave been successfully read from file
    const newRoutes = {};
    const partners = container.get('partners');

    Object.keys(partners).forEach(route => {

      if (newRoutes[route]) {
        console.error(`Error: duplicate route declaration. Route - ${route}, partner - ${partners[route].partner_name}`);
        return;
      }

      if (partners[route].default_handling)  {
        newRoutes[route] = handleDefaultPartner; //handle as default partner
      } else {
        //if we have custom function registered for this partner then we should use it, otherwise - use default
        if (!customPartnersRoutesHandlers[route]) {
          console.error(`Error: you have custom handled partner '${partners[route].partner_name}'`
            + ` but no custom route handler registered. Default handling will be applied.`);
          newRoutes[route] = handleDefaultPartner;

        } else {
          newRoutes[route] = customPartnersRoutesHandlers[route];
        }
      }
    });

    partnersRoutes = newRoutes;
  }
};

