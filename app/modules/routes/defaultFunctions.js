'use strict';

const fs = require('fs');
const config = require('../../../config/config');
const cookies = require('./../cookies');
const cache = require('./../cache');
const storage = require('./../storage');
const pixelImg = fs.readFileSync(__dirname + '/../../../assets/images/pixel.gif');
const { isValidPartnerUid, isValidUrl, hasNeededMacroses } = require('../../libs/validator');
const { USER_SYNC, PARTNER } = require('../../libs/constants').mappingSides;
const keyCrypt = require('../../libs/keyCrypt');

module.exports = {
  prepareCookies,
  handleUser,
  handleDefaultPartner
};

function prepareCookies(req, res, partner) {
  const { isExists, cookieValue } = cookies.getCookieInfo(req, config.cookie_key, partner.partner_name);

  if (isExists) {
    cache.incrExistUsers(partner.id);
  } else {
    cache.incrNewUsers(partner.id);
  }

  if (cookieValue) {
    res.setHeader("Set-Cookie", `${config.cookie_key}=${cookieValue}; path=/; domain=${config.cookie_domain}; `
      + `expires=${cookies.getDateExpiry()}`);
  }

  return cookieValue;
}

function handleUser(partner, urlParsed, cookieValue, callback) {
  if (!cookieValue) return callback(cookieValue);

  if (partner.keys_store_side === USER_SYNC) {
    const partnerUidParamName = partner.additional_info.PARTNER_UID_PARAM_NAME || 'puid';
    const partnerUid = urlParsed.query[partnerUidParamName]; //partner id example - qwert1234

    if (isValidPartnerUid(partnerUid)) {
      storage.savePartnerRef({partnerId: partner.id, partnerUid, uid: cookieValue})
        .then(storage.saveAppRef)
        .then(() => {
          return callback(cookieValue);
        })
        .catch((err) => {
          console.error(err);
          cache.incrErrors(partner.id);
          return callback(cookieValue);
        });

    } else {
      cache.incrInvalidKeys(partner.id);
      return callback(cookieValue);
    }

  } else if (partner.keys_store_side === PARTNER) {
    const keyForPartner = encodeURIComponent(keyCrypt.encryptKey(cookieValue, partner));
    return callback(keyForPartner);

  } else {
    //500, please contact tech department
    console.error('Error: no keys_store_side configured for partner ' + partner.partner_name);
    return callback(cookieValue);
  }
}

function handleDefaultPartner(req, res, partner) {
  const cookieValue = prepareCookies(req, res, partner);

  handleUser(partner, req.urlParsed, cookieValue, (keyForPartner) => {
    let statusCode = 200;
    let data = '';

    const headers = { //no cache headers by default
      "Cache-Control": "no-cache, no-store, must-revalidate",
      "Pragma": "no-cache",
      "Expires": "0"
    };

    let redirectUrl = getRedirectUrl(partner, req.urlParsed, req.protocol);

    if (redirectUrl && keyForPartner) {
      const macros = partner.additional_info.UUID_MACROS || config.default_uid_macros;
      if (isValidUrl(redirectUrl) && hasNeededMacroses(redirectUrl, [macros], partner)) {
        redirectUrl = redirectUrl.replace(macros, keyForPartner);
        headers['Location'] = redirectUrl;
        statusCode = 302;

      } else {
        cache.incrInvalidRedirs(partner.id);
        data = pixelImg;
        headers['Content-Type'] = 'image/gif';
      }
    } else {
      data = pixelImg;
      headers['Content-Type'] = 'image/gif';
    }

    try {
      res.writeHead(statusCode, headers);
    } catch (err) {
      console.error(`Error write head: partner - ${partner.name}, headers - ${JSON.stringify(headers)}`, err);
    }

    res.end(data);
  });
}

function getRedirectUrl(partner, urlParsed, reqProtocol) {
  if (partner.keys_store_side === USER_SYNC) return null;

  let redirectUrl = '';
  if (partner.partner_url) {
    redirectUrl = partner.partner_url;
  } else {
    const redirectUrlParamName = partner.additional_info.REDIRECT_URL_PARAM_NAME || 'redir';
    try {
      redirectUrl = decodeURIComponent(urlParsed.query[redirectUrlParamName]);
    } catch (err){
      console.log(err);
    }
  }

  if (!redirectUrl) return '';

  if (redirectUrl.startsWith('//'))
      redirectUrl = reqProtocol + ':' + redirectUrl;

  return redirectUrl;
}