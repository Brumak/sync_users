'use strict';

const container = {
  partners: {},
  syncedPixelsMurkup: {}
};

module.exports = {
  get(key) {
    return container[key];
  },

  set(key, value) {
    container[key] = value;
  }
};