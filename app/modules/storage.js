'use strict';

const config = require('../../config/config');
const storageConf = config.storage;
const Aerospike = require('aerospike');
const client = Aerospike.client(storageConf.connection);

const pm2 = require('pm2');

client.connect(err => {
  if (err) {
    console.error('Error connecting to aerospike cluster.', err);
    client.close();
  }
});

module.exports = {
  savePartnerRef(args = {}) {

    const { partnerId, partnerUid, uid } = args;
    return new Promise((resolve, reject) => {

      const key = new Aerospike.Key(storageConf.partnersNamespace, 'default', partnerId + config.partner_ref_delimiter + partnerUid);

      client.get(key, (err, record) => { //read first (read operations uses RAM, no ssd)
        let isSaveRequired = false;

        if (err) {
          if (err.code !== Aerospike.status.AEROSPIKE_ERR_RECORD_NOT_FOUND) { //means we have error different from not found
            console.error('Error while getting partnerId + partnerUid from storage.');
            return reject(err);
          }

          record = {uid};
          isSaveRequired = true;
        } else {
          if (record.uid !== uid) {
            record.uid = uid;
            isSaveRequired = true;
          }
        }

        if (!isSaveRequired) {
          updateExpiryDate(key, storageConf.ttl);
          return resolve(args);
        }

        const policy = { exists: Aerospike.policy.exists.CREATE_OR_REPLACE };
        const meta = { ttl: storageConf.ttl };

        client.put(key, record, meta, policy, (err) => {
          if (err) {
            console.error('Error while putting partnerUid to storage.');
            return reject(err);
          }

          return resolve(args);
        });
      });
    });
  },

  saveAppRef({ partnerId, partnerUid, uid } = {}) {
    return new Promise((resolve, reject) => {

      const key = new Aerospike.Key(storageConf.appNamespace, 'default', uid);
      client.get(key, (err, record) => {
        if (err) {
          if (err.code === Aerospike.status.AEROSPIKE_ERR_RECORD_NOT_FOUND) {
            record = {};
          }
          else {
            console.error('Error while getting internal uid from storage.');
            return reject(err);
          }
        }

        const keys = record.keys || {};

        if (!keys[partnerId] || keys[partnerId] !== partnerUid) { //if not exists or have been changed then update

          //if partner sent new partnerUid for existing user - we must delete old partner reference
          if (keys[partnerId] && keys[partnerId] !== partnerUid) {
            const oldPartnerRef = partnerId + config.partner_ref_delimiter + keys[partnerId];
            removeOldPartnerRef(oldPartnerRef);
          }

          keys[partnerId] = partnerUid;
          record.keys = keys;

          const policy = { exists: Aerospike.policy.exists.CREATE_OR_REPLACE };
          const meta = { ttl: storageConf.ttl };

          client.put(key, record, meta, policy, (err) => {
            if (err) {
              console.error('Error while putting internal uid to storage.');
              return reject(err);
            }

            return resolve(true);
          });
        } else {
          updateExpiryDate(key, storageConf.ttl);
          return resolve(true);
        }
      });
    });
  },

  close() {
    client.close();
  },
};

function updateExpiryDate(key, ttl) {
  const ops = [Aerospike.operations.touch(ttl)];

  client.operate(key, ops, function (err) {
    if (err && err.code !== Aerospike.status.AEROSPIKE_ERR_RECORD_NOT_FOUND) {
      console.error('Error while setting updating ttl.', err);
    }
  });
}

function removeOldPartnerRef(oldKey) { //we should set expiry 1 to old partner's ref if new one was created
  //we do not use remove because remove has more overhead
  const key = new Aerospike.Key(storageConf.partnersNamespace, 'default', oldKey);
  updateExpiryDate(key, 1);
}